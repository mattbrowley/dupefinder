===================================
DupeFinder.py
===================================

DupeFinder.py is a python script which will find duplicate files
within a directory tree and present them to the user to delete or
keep. Regardless of how many files share the same checksum, they
are presented in pairs so that the user is always presented with
only three options (keep 1, keep 2, keep both). The files are
presented in a way that ensures any combination of keep/delete 
decisions is possible for a set of duplicates. If no arguments
are passed then the script will search from within the current
working directory, so place it in the folder you wish to search.
Alternatively, you can give as arguments the folder or folders 
you wish to include in the search. Additionally, you can pass the
"--find" argument to simply list the duplicates without ever
deleting any of them.


===================================
DupeFinder_Graphic.py
===================================

DupeFinder_Graphic.py takes all the code from DupeFinder.py and
adds the feature of displaying the duplicate images side by side
so the user can verify that they are duplicates. The user input
is still done through a command line interface. It requires the
cv2 and numpy modules to do this. cv2 is included since it is 
easy to include and not as commonly installed. Numpy is a more
substantial and more common module so it is left to the user to
ensure that it is installed on their system. 
